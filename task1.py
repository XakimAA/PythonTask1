# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 19:24:34 2018

@author: Анжелика
"""
import math
class Arith_fract():
    sign = ''
    numerator = 0
    denominator = 0

    def __init__(self, num, den):
        self.numerator = num
        self.denominator = den
    
    def common_denom(self, other):
        if (self.denominator != other.denominator) :
            num1 = self.numerator * other.denominator
            num2 = other.numerator * self.denominator
            den = self.denominator * other.denominator
        else: 
            num1 = self.numerator
            num2 = other.numerator 
            den = other.denominator
        return num1, num2, den
    def __add__(self, other):
        num1, num2, den = self.common_denom(other)
        return Arith_fract(num1 + num2, den)
        
    def __sub__(self, other):
        num1, num2, den = self.common_denom(other)
        return Arith_fract(num1 - num2, den)
    def __mul__(self, other):
        return Arith_fract(self.numerator * other.numerator, self.denominator * other.denominator)
    
    def __truediv__(self, other):
        return Arith_fract(self.numerator * other.denominator, self.denominator *other.numerator )
    
    def __str__(self):
        if (self.numerator < 0):
            sign = '-'
            num = int(math.fabs(self.numerator))
        else:
            sign = ''
            num = self.numerator  
        if (num % self.denominator == 0):
            c = num // self.denominator
            print (str(c))
        elif (num < self.denominator): 
            print (sign + "(" + str(self.numerator) + "/" + str(self.denominator) + ")")
        else:
            c = num // self.denominator
            v = num % self.denominator     
            print (sign + str(c) + "(" + str(v) + "/" + str(self.denominator) + ")")


def input(file):
        st = file.read()
        print(st)
        list = st.split(' ')
        n, d = to_Fraction(list[0])
        a = Arith_fract(n,d)
        n, d = to_Fraction(list[2])
        b = Arith_fract(n,d)
        if list[1] == '+':
            c = a + b
        elif list [1] == '-':
            c = a - b
        elif list[1] == '*':
            c = a * b
        elif list[1] == '/':
            c = a / b
        else: print ("Что-то пошло не так...")
        c.__str__()

def to_Fraction(integer):
        list = integer.partition('(')
        c = list[0]
        list = list[2].split(')')
        list = list[0].split('/')
        if (c == ''): 
            c = 0
        return int(c)*int(list[1])+int(list[0]), int(list[1]) 

f = open("input.txt",'r')
input(f)
